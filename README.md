# D20Network Blockchain

## Introduction

Welcome to the D20Network Blockchain repository, a groundbreaking initiative that merges the realms of tabletop role-playing games (TTRPGs) and cutting-edge technology. This project is a brainchild of Void, Corp, a Utah-based software and hardware development company with a passion for innovation and a vision to redefine the gaming landscape.

Our mission with the D20Network is to leverage the power of blockchain technology and artificial intelligence (AI) to transform the TTRPG experience. We believe in the potential of these technologies to create a more immersive, engaging, and streamlined gaming environment.

TTRPGs, at their core, are about storytelling and character development. However, the process of character creation and management can often be complex and time-consuming, especially for newcomers to the genre. Our platform aims to simplify this process, making TTRPGs more accessible to a wider audience.

By utilizing web 3.0 domains for data storage and viewing, we aim to enhance efficiency and immersion in the gaming experience. Furthermore, our use of AI opens up new possibilities for solo players, allowing them to play, develop, and grow in ways previously unimaginable.

Join us on this exciting journey as we explore the frontiers of technology and gaming, and redefine what it means to play TTRPGs in the digital age.

## Overview

Tabletop Role-Playing Games (TTRPGs) are a unique form of entertainment that combine storytelling, strategy, and social interaction. Players assume the roles of characters in a fictional setting and, guided by a Dungeon Master (DM), navigate through a shared narrative. The outcome of character actions and events is often determined by dice rolls, adding an element of chance to the game.

However, TTRPGs can be complex and time-consuming, particularly when it comes to character creation and management. This complexity can be a barrier to entry for new players and can slow down gameplay for experienced players.

The D20Network aims to address these challenges by leveraging the power of blockchain technology and artificial intelligence (AI). By storing character data on a blockchain, we can ensure that character information is secure, transparent, and easily accessible. This not only simplifies character management but also enhances the integrity of the game by preventing unauthorized modifications.

In addition, we are exploring the use of AI to support solo gameplay. Traditionally, TTRPGs require a group of players and a DM. However, by using AI, we can simulate the role of the DM, allowing solo players to enjoy the game at their own pace.

Furthermore, our platform utilizes web 3.0 domains for data storage and viewing. This allows for a more efficient and immersive gaming experience, as players can easily access and interact with their character data in real-time.

In essence, the D20Network is not just about enhancing the TTRPG experience. It's about pushing the boundaries of what's possible in the world of gaming. It's about using technology to create a more inclusive, accessible, and engaging gaming environment. And most importantly, it's about bringing the magic of TTRPGs to a wider audience.



## Background

The D20Network is built on two key technologies: blockchain and artificial intelligence (AI). Both of these technologies have seen significant advancements in recent years and have the potential to revolutionize a wide range of industries.

**Blockchain Technology**

Blockchain technology is a type of distributed ledger that allows data to be stored across a network of computers. This data is stored in blocks, and each block is linked to the one before it, forming a chain. The decentralized nature of blockchain technology ensures that no single entity has control over the entire network, enhancing security and transparency.

In the context of the D20Network, blockchain technology is used to store character data. This ensures that character information is secure and cannot be tampered with. Additionally, the transparent nature of blockchain technology allows players to verify their character data, enhancing trust in the gaming experience.

**Artificial Intelligence (AI)**

AI is a branch of computer science that aims to create machines that mimic human intelligence. This can include tasks such as learning, reasoning, problem-solving, perception, and language understanding.

In the D20Network, AI is used to support solo gameplay. Traditionally, TTRPGs require a group of players and a Dungeon Master (DM). However, by using AI, we can simulate the role of the DM, allowing solo players to enjoy the game at their own pace.

**Web 3.0 Domains**

Web 3.0, also known as the semantic web, represents the next stage in the evolution of the internet. It aims to create a web environment where data is connected, open, and easily accessible.

In the D20Network, we utilize web 3.0 domains for data storage and viewing. This allows for a more efficient and immersive gaming experience, as players can easily access and interact with their character data in real-time.

## The Chain Details

Algorithm :Scrypt Proof of Work
Coin name: D20Network
Coin abbreviation: D20
Public address letter V: Public address letter testnet :2

RPC port: 17461
P2P port: 17462

Block reward :50 coins 
Block halving 210000 blocks
Coin supply: 21000000 coins

Coinbase maturity: 10 ( + 1 default confirmation) blocks
Target spacing: 3 minutes
Target timespan: 9 minutes
Transaction confirmations: 6 blocks


Windows Auto Install & Mine

Install and mine for blocks automatically with your Windows wallet. The tutorial is compatible with Windows 10 and above.
Download the file d20network-auto.zip. Open File Explorer and go to your downloads directory. Right click the zip file d20network-auto.zip and select "Properties".

Select "Unblock". Press the button "OK".

Extract the zip file d20network-auto.zip

Execute install.bat to automatically install your wallet and mine your first block.

----------------------------------------
Linux: compatible with Ubuntu Desktop 22.04 and above.


Download the file d20network-auto.sh and open a Terminal window.

Make the install file executable with the following command:

chmod +x $HOME/Downloads/d20network-auto.sh

Open Files and go to your Downloads directory. Select the file d20network-auto.sh, press the right button of your mouse and click on "Run as a Program" to automatically install your wallet and mine your first block.

Enter your Ubuntu user password when requested.
